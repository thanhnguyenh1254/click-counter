$(function () {

  var select = document.querySelector(".employees");
  var lastSelected = localStorage.getItem("select");
  var theDate = localStorage.getItem("theDate");

  // For the home page
  if ($('body').hasClass('home-page')) {
    $('form').on('submit', function (e) {
      e.preventDefault();
      // Check if a name is selected
      if ($('#employees-id :selected').text() == 'Please select a name' || '') {

        alert('Error: no name selected.');

      } else { // Send the data to server file
        $.ajax({
          type: 'post',
          url: 'index.php',
          data: $('form').serialize(),

        }).done(function () {
          $.get("functions/day.php", function (response) {
            $("#number").text(response);
          });

          $.get("functions/week.php", function (response) {
            $("#number1").text(response);
          });
        });
      }
    })

    if (lastSelected) {
      select.value = lastSelected;
    }
    //Local Storage
    select.onchange = function () {
      lastSelected = select.options[select.selectedIndex].value;
      localStorage.setItem('select', lastSelected);
    }

    timeRefresh();
  } else if ($('body').hasClass('history-page')) { //For the history page

    $('form').on('submit', function (e) {
      e.preventDefault();

      // Check if a date is selected
      if ($('#date-picker').val() == "") {
        alert('Error: no date selected.');
      } else {
        var data = $("#date-picker").val();
        var dataToSend = 'myDate=' + data;
  
        $.ajax({ // Send the data to the server file
          type: "POST",
          url: 'functions/history-retriever.php',
          data: dataToSend,
          async: true,
          success: function (data) {
            var str = data.split("&");
            $("#number2").text(str[0]);
            $("#number3").text(str[1]);
          }
        });
      }
    })

    if (theDate) {
      $("#date-picker").val(theDate);
    }
    // Local storage
    $("#date-picker").change(function () {
      if (typeof (Storage) !== "undefined") {
        localStorage.setItem("theDate", $("#date-picker").val());
      }
    });
  }

  // Refresh of 60 * 1000 interval
  function timeRefresh() {

    $.get("functions/day.php", function (response) {
      $("#number").text(response);
    });

    $.get("functions/week.php", function (response) {
      $("#number1").text(response);
    });

    setTimeout(timeRefresh, 60 * 1000)
  }

})