<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF=8">
  <link rel="shortcut icon" href="https://idbbn.fi/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Just a click-du-ba-click page">
  <title>Click-du-ba-click</title>
  <META NAME="robots" CONTENT="noindex, nofollow">
  <link rel="stylesheet" type="text/css" href="css/click-counter.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/click-counter.js"></script>

</head>

<?php include './functions/connection.php'; ?>


<body class="home-page">
<form action="#" method="POST" class="form-home">
    <div class="wrapper">
        <ul>
            <li><a class="active" href="#">Home</a></li>
            <li><a href="./history.php">History</a></li>
        </ul>

        <div class="content">
            <div class="centered">
                <p id="number"></p>
                <p id="number1"></p>
                <div class="name-wrapper">
                    <span>Name:</span>
                    <select id="employees-id" class="employees" name="employees">
                        <option value="">Please select a name</option>
                        <option value="Timo Kruskopf">Timo Kruskopf</option>
                        <option value="Matti Aalto-setala">Matti Aalto-Setälä</option>
                        <option value="Matti Airas">Matti Airas</option>
                        <option value="Susanna Juusti">Susanna Juusti</option>
                        <option value="Laura Heino-Laurila">Laura Heino-Laurila</option>
                        <option value="Marjut Herranen">Marjut Herranen</option>
                        <option value="Nana Hjelt">Nana Hjelt</option>
                        <option value="Tanja Holm">Tanja Holm</option>
                        <option value="Saara Parikka">Saara Parikka</option>
                        <option value="Niina Sariola">Niina Sariola</option>
                        <option value="Jussi Soro">Jussi Soro</option>
                        <option value="Karin Tammi">Karin Tammi</option>
                        <option value="Ville Murtojarvi">Ville Murtojärvi</option>
                        <option value="Jere Hakamaki">Jere Hakamäki</option>
                        <option value="linda hagerstrand">Linda Hägerstrand</option>
                        <option value="Sari Jalava">Sari Jalava</option>
                        <option value="Tanja Kalliala">Tanja Kalliala</option>
                        <option value="Matti Kemppainen">Matti Kemppainen</option>
                        <option value="Lauri Lonn">Lauri Lönn</option>
                        <option value="Julius Murtojarvi">Julius Murtojärvi</option>
                        <option value="Tommi Selander">Tommi Selander</option>
                        <option value="Eija Väliranta">Eija Väliranta</option>
                        <option value="Henrik Lagercrantz">Henrik Lagercrantz</option>
                        <option value="Jani Aaltonen">Jani Aaltonen</option>
                        <option value="Markku Alikoski">Markku Alikoski</option>
                        <option value="Axel engberg">Axel Engberg</option>
                        <option value="Miko Lehtonen">Miko Lehtonen</option>
                        <option value="Joonas-Lieppinen-Trujillo">Joonas Lieppinen-Trujillo</option>
                        <option value="Jessi Maenpaa">Jessi Mäenpää</option>
                        <option value="Joonas Makilä">Joonas Mäkilä</option>
                        <option value="Robert Nygard">Robert Nygård</option>
                        <option value="Daniel Prague">Daniel Prange</option>
                        <option value="Tero Rantaruikka">Tero Rantaruikka</option>
                        <option value="Darren Richardson">Darren Richardson</option>
                        <option value="Jukka Riihimaki">Jukka Riihimäki</option>
                        <option value="Veneta Stefanova">Veneta Stefanova</option>
                        <option value="Antti Sulkakoski">Antti Sulkakoski</option>
                        <option value="Jouni Toivonen">Jouni Toivonen</option>
                        <option value="Tiia Toivonen">Tiia Toivonen</option>
                        <option value="Jari Vuomajoki">Jari Vuomajoki</option>
                        <option value="Markku Ylitalo">Markku Ylitalo</option>
                    </select>
                </div>
                <input type="submit" class="button" value="INTERRUPTED!">
            </div>
        </div>

    </div>
    </form>
</body>
</html>