CREATE TABLE employeeclicks (
 clickid INT NOT NULL AUTO_INCREMENT,
 employeename VARCHAR(255) NOT NULL,
 clicktime DATETIME NOT NULL,
 PRIMARY KEY (clickid));