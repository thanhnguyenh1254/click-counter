<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF=8">
  <link rel="shortcut icon" href="https://idbbn.fi/sites/default/files/favicon.ico" type="image/vnd.microsoft.icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Just a click-du-ba-click page">
  <title>Click-du-ba-click</title>
  <META NAME="robots" CONTENT="noindex, nofollow">
  <link rel="stylesheet" type="text/css" href="css/click-counter.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/click-counter.js"></script>

</head>

<body class="history-page">

    <form action="" method="POSt" class="form-history">
        <div class="wrapper">
            <ul>
                <li><a href="./index.php">Home</a></li>
                <li><a class="active" href="#">History</a></li>
            </ul>
        
            <div class="content">
                <p> Enter Date: </p>
                <p class="results"> </p>
                <input id="date-picker" type="date" name="date-picker">
                <input class="date-submit" type="submit" value="Submit">
                <div class="centered">
                    <p id="number2"> </p>
                    <p id="number3"></p>
                </div>
            </div>

        </div>
    </form>

</body>
</html>